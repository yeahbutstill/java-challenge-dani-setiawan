package ist.challenge.dani_setiawan.repository;

import ist.challenge.dani_setiawan.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
