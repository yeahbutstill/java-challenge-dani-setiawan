package ist.challenge.dani_setiawan.enumeration;

public enum Status {
    SUCCESS,
    USER_ALREADY_EXISTS,
    FAILURE
}
