## Setup Database

* Run PostgreSQL di Docker

    ```bashpro shell script
    docker run --rm \
    --name java-challenge-dani-setiawan \
    -e POSTGRES_DB=istdb \
    -e POSTGRES_USER=istdnis \
    -e POSTGRES_PASSWORD=PNSJkxXvVNDAhePMuExTBuRR \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v "$PWD/istdb-data:/var/lib/postgresql/data" \
    -p 5432:5432 \
    postgres:13
   ```

* Login PostgreSQL
  ```shell
    psql -h 127.0.0.1 -U istdnis istdb
  ```
  
